from django.db import models

# Create your models here.


class Count(models.Model):

    _count = models.IntegerField(default=0)

    def incrementCount(self):
        self._count += 1

    def getCount(self):
        return self._count
