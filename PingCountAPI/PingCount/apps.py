from django.apps import AppConfig


class PingcountConfig(AppConfig):
    name = 'PingCount'
