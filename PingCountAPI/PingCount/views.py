# Create your views here.
import logging

from django.http import HttpResponse, JsonResponse
from PingCount.models import Count

countClass = Count()


def index(request):
    return HttpResponse("Bienvenue sur PingCount. Les commandes à disposition sont les suivantes : /ping et /count")


def ping(request):
    countClass.incrementCount()
    return JsonResponse({'message': 'pong'})


def count(request):
    return JsonResponse({'pingCount': countClass.getCount()})
