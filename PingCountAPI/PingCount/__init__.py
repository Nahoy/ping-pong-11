import logging

logger = logging.getLogger(__name__)
logger.setLevel('INFO')
logger.info('PingCount est démarré', exc_info=True)