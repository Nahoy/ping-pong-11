from django.core.serializers import json
from django.test import TestCase, Client

# Create your tests here.
from django.urls import reverse


class SimpleTest(TestCase):
    def test_ping_status(self):
        response = self.client.get(reverse('ping'))
        self.assertEqual(response.status_code, 200)

    def test_ping_message(self):
        response = self.client.get(reverse('ping'))
        self.assertEqual(response.json()['message'], 'pong')

    def test_count_status(self):
        response = self.client.get(reverse('count'))
        self.assertEqual(response.status_code, 200)
