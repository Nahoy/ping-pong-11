import logging
import signal
import time
import sys


def handler(signum, frame):
    logger = logging.getLogger(__name__)
    logger.error('PingCount est arrétée', exc_info=True)
    time.sleep(10)
    sys.exit(0)

signal.signal(signal.SIGINT, handler)
