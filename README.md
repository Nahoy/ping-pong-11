# PingCount API - Group 11 [![build status](https://gitlab.com/Nahoy/ping-pong-11/badges/master/build.svg)](https://gitlab.com/Nahoy/ping-pong-11/commits/master)

# Installation

1. Install python 3.5+ `sudo apt-get install python3`

2. Install pip `sudo easy_install pip`

3. Install requierements `python3 -m pip install -r requirements.txt`

4. Change Sentry DSN `find . -type f -exec sed -i 's#YOUR_DSN_CLIENT_KEY#g' {} \;` 

# Running server

## With local installation

Use virtualenv to run python command (see 7)
	
`python3 pathOfYourFile/manage.py runserver`

**Find the address at** http://localhost:8080/

Use command /ping and /count

Optionally you can give port and host
	
`python3 pathOfYourFile/manage.py runserver 172.16.203.1:8080`

## With Docker

Make sure docker is installed first `docker info`

Pull image `docker pull registry.gitlab.com/nahoy/ping-pong-11/app`

Then run it `docker run --rm -p 8000:8000 registry.gitlab.com/nahoy/ping-pong-11/app`

**Find the address at** http://docker-container-IP:8000/

# Testing

Install jq `sudo apt-get install jq`

Install curl `sudo apt-get install curl`

**Test the api**

Don't forget to set the permission with `chmod 755`

`./test-U.sh`

You can specify the host and port optionally (default is 127.0.0.1:8000)

`./test-U.sh 127.0.0.1 8080`

**Or use docker compose**

Check if docker-compose is installed `docker-compose --version`

Then run test with `docker-compose up`