#!/bin/bash

# Indique au système que l'argument qui suit est le programme utilisé pour exécuter ce fichier
# En règle générale, les "#" servent à mettre en commentaire le texte qui suit comme ici

if [ -z "$1" ]
then
	host='127.0.0.1'
else
	host="$1"
fi

if [ -z "$2" ]
then
	port='8000'
else
	port=$2
fi

BASE_URL="$host:$port"

echo
eval "echo Testing on: $BASE_URL ..."
echo
sleep 6s

validity_counter=0

# Count à 0
test_count_init=$(curl -s $BASE_URL/count | jq -r '.pingCount')
if [ "$test_count_init" = "0" ]
then
  echo "OK :  count = $test_count_init"
  ((validity_counter++))
else
  echo "Erreur : /count $test_count_init au lieu de 0"
fi

# Ping message = Pong
test_ping_msg=$(curl -s $BASE_URL/ping | jq -r '.message')
if [ "$test_ping_msg" = "pong" ]
then
  echo "OK :  pong = $test_ping_msg"
  ((validity_counter++))
else
  echo "Erreur: /ping renvoie $test_ping_msg au lieu de 'pong'"
fi

# Count à 1
test_count_1=$(curl -s $BASE_URL/count | jq -r '.pingCount')
if [ "$test_count_1" = "1" ]
then
  echo "OK :  count = $test_count_1"
  ((validity_counter++))
else
  echo "Erreur: /count renvoie $test_count_1 au lieu de 1"
fi

if [ $validity_counter -eq 3 ]
then
	echo "Tous les tests OK $validity_counter/3"
	exit 0
else
	echo "Test incomplet : $validity_counter/3"
	exit 1
fi